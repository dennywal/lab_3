Author:Dennis Walsh

This was a lab in which I calibrated a charged coupled device on a telescope for use in future observations.
I took bias and flat images and used the data to reduce future science images.

This lab is written in an ipython (jupyter) notebook and requires the numpy, scipy and matplotlib libraries.